import { LitElement, html } from 'lit-element';

class PersonaStats extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();
        this.people = [];
        
    }

    getPeopleArrayInfo(people){
        console.log("GetPeopleArrayInfo");

        //Object donde podemos añadir propiedades
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;

        return peopleStats;

    }
    updated(changedProperties) {
        console.log("PERSONA STATS: updated");

        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad PEOPLE en persona-stats");
            //Calculamos la longitud del array
            let peopleStats = this.getPeopleArrayInfo(this.people);
            console.log ("People len: "+ peopleStats.numberOfPeople);
            
              //Mandando info a PersonaMain //
            this.dispatchEvent(new CustomEvent("updated-people-stats", {
                detail: {
                    peopleStats: peopleStats
                }
            }));
        }
    }
    
        
    
}

customElements.define("persona-stats", PersonaStats);