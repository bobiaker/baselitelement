import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';


class PersonaApp extends LitElement {

    static get properties() {
        return{  
            people: {type: Array}          
        };
    }

    constructor() {
        super();
        
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar @new-person="${this.newPerson}" @new-range-filter="${this.newRangeFilter}" class="col-2"></persona-sidebar>
                <persona-main @updated-people="${this.updatePeople}" class="col-10"></persona-main>
            </div>
          
           
            <persona-footer></persona-footer>
            <persona-stats  @updated-people-stats="${this.updatePeopleStats}"></persona-stats>
        `;
    }

   

    updated(changedProperties) {
        console.log("PERSONA APP: updated");

        // Control de si se muestra la pantalla de formulario o la del listado de personas //
        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad PEOPLE en persona-app");
            console.log(this.people)
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    newRangeFilter (e)
    {
        console.log("PERSONA APP: newRangeFilter");
        console.log("   >> newRangeFilter: "+ e.detail.rangeYears);
        
        //Es necesario enviar esta informacion a PERSONA-MAIN para que no muestre las personas con mas edad de la indicada
        //Para enviar informacion a otros componentes aguas abajo modificamos sus propiedades
        this.shadowRoot.querySelector("persona-main").rangeYears = e.detail.rangeYears; 
    }

    updatePeople (e) {
        console.log("PERSONA APP: updatePeople");
        console.log(e.detail);
        
        this.people = e.detail.people;
        
    }

    updatePeopleStats (e){
        console.log("PERSONA APP: updatePeopleStats");
        console.log(e.detail);
        console.log(e.detail.peopleStats);
        
        //this.people = e.detail.people; 
        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
    }

    newPerson(e) {
        console.log("newPerson en persona-app");
        // cambiar valor propiedad -> true/false //
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
        // también se ha de poner a false en el constructor

    }
}

customElements.define("persona-app", PersonaApp);