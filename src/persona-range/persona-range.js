import { LitElement, html } from 'lit-element';

class PersonaRange extends LitElement {

    static get properties() {
        return {
            rangeYears: {type: Number}
        };
    }

    constructor() {
        super();
        this.rangeYears = 100;
    }

    updated(changedProperties) {
        console.log("PERSONA RANGE: updated");

        //Recibimos el valor del RANGO:
        if(changedProperties.has("rangeYears")){
            console.log("   >> Ha cambiado el valor de la propiedad rangeYears en persona-range");
            console.log("   >> "+this.rangeYears);

            //Esto en realidad ni hace falta porque el OUTPUT tag ya lo estamos alimentando con la propiedad, y lo coge sobre la marcha
            //this.shadowRoot.getElementById("numYears").value = this.rangeYears;

            //Elevamos la notificacion via evento al Persona-SideBar, que a su vez se lo pasara a Persona-APP
            this.dispatchEvent(new CustomEvent("persona-range-update", {
                detail: {
                    rangeYears: this.rangeYears
                    }
            }));

        }


    }


    updateRange(e)
    {
        //Actualizamos el valor del RANGO
        console.log("PERSONA RANGE: updateRange");
        console.log(e.target.value);
        this.rangeYears = e.target.value;

        

    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
                <p>
                <div>
                    <label for="volume">RangoAños</label>   
                    <input type="range" id="years" name="years" min="1" max="100" 
                            @input="${this.updateRange}" 
                            .value="${this.rangeYears}"/>

                    <output id="numYears" name="numYears" .value="${this.rangeYears}">0</output>
                </div>                    
        `;
    }

    
}

customElements.define("persona-range", PersonaRange);