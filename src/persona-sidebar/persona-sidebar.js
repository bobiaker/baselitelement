import { LitElement, html } from 'lit-element';
import '../persona-range/persona-range.js';

class PersonaSidebar extends LitElement {

    static get properties() {
        return {
            peopleStats: {type: Object}
        };
    }

    constructor() {
        super();
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <aside>
                <section>
                    <div>
                        Hay <span class="badge bp-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas
                    </div>
                    <persona-range @persona-range-update="${this.updateRangeFilter}"></persona-range>
                    <div class="mt-5">
                        <button @click=${this.newPerson} class="w-100 btn bg-success" style="font-size: 50px"><strong>+</strong>
                    </div>
                </section>
            </aside>
        `;
    }

    updateRangeFilter(e)
    {
        console.log("PERSONA SIDEBAR: updateRangeFilter");
        console.log("   >> Enviadno a PERSONA-APP el nuevo filtro-rango de personas: "+e.detail.rangeYears);

        this.dispatchEvent(new CustomEvent("new-range-filter", {
                                    detail: {
                                            rangeYears: e.detail.rangeYears
                                            }
                                        }));
    }
    newPerson(e) {
        console.log("newPerson en persona-sidebar");
        console.log("Se va a crear una nueva persona");

        this.dispatchEvent(new CustomEvent("new-person", {}));
    }
}

customElements.define("persona-sidebar", PersonaSidebar);