import { LitElement, html } from 'lit-element';

class PersonaMainDM extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
        };
    }

  
    constructor() {
        super();
        console.log("PERSONA MAIN DM: Constructor");
        this.people = [
            {   name: "Sauron", yearsInCompany: 100,  photo: {src: "./img/sauron.jpg",alt: "Sauron"}, profile: "Lore Ipsum"}, 
            {   name: "Gandalf", yearsInCompany: 90,  photo: {src: "./img/gandalf.png",alt: "Gandalf"}, profile: "Lore Ipsum"}, 
            {   name: "Gollum", yearsInCompany: 40,  photo: {src: "./img/gollum.jpg",alt: "Gollum"}, profile: "Lore Ipsum"}, 
            {   name: "Frodo", yearsInCompany: 20,  photo: {src: "./img/frodo.jpg",alt: "Frodo"}, profile: "Lore Ipsum"}, 
        ]
    }

    updated(changedProperties) {
        console.log("PERSONA MAIN DM: updated");

        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad PEOPLE en persona-main-dm");
            console.log(this.people);

            //Mandando info a PersonaAPP //
           this.dispatchEvent(new CustomEvent("persona-main-dm", {
            detail: {
                people: this.people
            }
        }));
        
        }
        

    }

    render(){
        return html``;
    }

    getPeopleData() {
        console.log("getPeopleData");
        console.log("Obteniendo datos de las personas");
       
        console.log("Fin de la getPeopleData");
    }

}

customElements.define("persona-main-dm", PersonaMainDM);