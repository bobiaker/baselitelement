import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {

    static get properties() {
        return {
            fname: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String}
        }
    }

    constructor() {
        super();

        this.fname = "Prueba nombre";
        this.yearsInCompany = 6;

    }

    updated(changedProperties) {
        changedProperties.forEach((oldValue, propName) => {
            console.log("Ha cambiado el valor de la propiedad " + propName + " anterior era " + oldValue);
        })

        if (changedProperties.has("fname")) {
            console.log("Propiedad Name ha cambiado de valor. El anterior era" + 
            changedProperties.get("fname") + ". El nuevo es " + this.fname);
        }

        if (changedProperties.has("yearsInCompany")) {
            console.log("Propiedad yearsInCompany ha cambiado de valor. El anterior era" + 
            changedProperties.get("yearsInCompany") + ". El nuevo es " + this.yearsInCompany);

            this.updatePersonInfo();
        }

    }

    render(){
        return html`
        <div>
            <label>Nombre Completo</label>
            <input type="text" id="fname" name="fname" value="${this.fname}" @input="${this.updateName}"></input>
            <br />
            <label>Años enla empresa</label>
            <input type="text" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
            <br />
            <input type "text" value="${this.personInfo}" disabled></input>
            <br />      
        </div>
        `;
    }

    updateName(e) {
        console.log("updateName");
        this.fname = e.target.value;
    }
 
    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo() {
        console.log("updatePersonInfo");
        
        if (this.yearsInCompany >= 7){
            this.personInfo = "lead";
        }    else if(this.yearsInCompany >= 5) {
            this.personInfo = "senior";
        }    else if(this.yearsInCompany >= 3) {
            this.personInfo = "team";
        }    else {
            this.personInfo = "junior";
        }
    }

}

customElements.define("ficha-persona", FichaPersona);